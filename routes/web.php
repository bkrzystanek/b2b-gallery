<?php

//Album
Route::get('/', 'AlbumController@getList')->name('index');
Route::get('/create-album', 'AlbumController@getForm')->name('album_create');
Route::post('/create-album', 'AlbumController@postForm')->name('album_create_post');
Route::get('/show-album/{id}', 'AlbumController@showAlbum')->name('album_show');
Route::post('/remove-album', 'AlbumController@remove')->name('album_remove');

//Photo
Route::get('/add-photo/album/{id}', 'PhotoController@getForm')->name('photo_add');
Route::post('/add-photo/album/{id}', 'PhotoController@postForm')->name('photo_add_post');


