@extends('main')
@section('title', 'Dodaj nowy album')

@section('content')
@if ($errors->any())
<div class="alert">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<form name="create_new_album" method="POST" action="">
    {{ csrf_field() }}
    <fieldset>
        <legend>Utwórz Album</legend>
        <div>
            <label for="name">Nazwa albumu</label> 
            <input name="name" type="text" placeholder="Nazwa" value="{{ old('name') }}">
        </div>
        <button type="submit">Zapisz!</button>
    </fieldset>
</form>
@include('tools.back2main')
@endsection