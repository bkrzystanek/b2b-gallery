@extends('main')
@section('title', 'Dodaj zdjęcie' )

@section('content')
@if ($errors->any())
    <div class="alert">
        <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
        </ul>
    </div>
@endif

@if ($album)
<form name="add_photo_to_album" method="POST" action="">
    {{ csrf_field() }}
    <fieldset>
        <input type="hidden" name="album_id" value="{{ $album->id }}" />
        
        <legend>Dodaj zdjęcie do Albumu ({{ $album->name }})</legend>
        <div>
            <label for="name">Adres do zdjęcia</label> 
            <input name="photo_url" type="text" placeholder="URL" value="{{ old('photo_url') }}">
        </div>
        <button type="submit">Zapisz!</button>
    </fieldset>
</form>
    @include('tools.back2', ['href' => URL::route('album_show', ['id' => $album->id]), 'title' => $album->name ])
    <br />
@else
    Nieprawidłowy Album <br />
@endif
    @include('tools.back2main')
@endsection