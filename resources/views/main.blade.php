<html>
    <head>
        <title>B2B Gallery - @yield('title')</title>
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: sans-serif;
                margin: 0;
            }
            
            th, td {
                font-weight: normal;
                border: 1px solid #eee;
            }
            
            .alert {
                color: red;
            }
        </style>
    </head>
    <body>
        <div class="container">
            @yield('content')
        </div>
    </body>
</html>