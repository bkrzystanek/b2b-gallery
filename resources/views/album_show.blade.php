@extends('main')
@section('title')
'Album ' {{ $album->name }}
@endsection

@section('content')
    @if(count($album->Photos) > 0)
        <p><b>Zdjęcia:</b></p>
        @each('photo_onlist', $album->Photos, 'photo')
    @else
        <p>Pusto ....</p>
    @endif
    
    <p><a href="{{ URL::route('photo_add', ['id' => $album->id]) }}">Dodaj zdjęcie</a></p>
    <p>@include('tools.back2main')</p>
@endsection