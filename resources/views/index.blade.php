@extends('main')
@section('title', 'Strona główna')

@section('content')
    @if(count($albums) > 0)
        <p><b>Aktualne albumy</b></p>
        <form method="POST" action="{{ URL::route('album_remove') }}">
            {{ csrf_field() }}
            <ul>
                @each('album_onlist', $albums, 'album')
            </ul>
        </form>
    @else
        Aktualnie brak albumów, <a href="{{ URL::route('album_create') }}">dodaj</a> jakiś :)
    @endif
        <p><a href="{{ URL::route('album_create') }}">Dodaj nowy album</a></p>
@endsection