<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AlbumRemovePost extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     * Must be true, for this public form.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'album_id' => 'required|numeric|exists:albums,id',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     * 
     * @return array
     */
    public function messages()
    {
        return [
            'album_id.required' => 'Nieprawidłowe żądanie',
            'album_id.numeric' => 'Nieprawidłowe żądanie',
            'album_id.exists' => 'Nieprawidłowe żądanie',
        ];
    }

}
