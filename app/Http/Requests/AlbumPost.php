<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AlbumPost extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     * Must be true, for this public form.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:255'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     * 
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Nazwa albumu jest wymagana',
            'name.min' => 'Nazwa albumu powinna mieć minimalnie 3 znaki',
            'name.max' => 'Nazwa albumu powinna mieć maksymalnie 255 znaków',
        ];
    }

}
