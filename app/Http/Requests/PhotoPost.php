<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PhotoPost extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     * Must be true, for public forms.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'album_id' => 'required|numeric|exists:albums,id',
            'photo_url' => 'required|url'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     * 
     * @return array
     */
    public function messages()
    {
        return [
            'album_id.required' => 'Nieprawidłowy identyfikator albumu',
            'album_id.numeric' => 'Nieprawidłowy identyfikator albumu',
            'album_id.exists' => 'Nieprawidłowy identyfikator albumu',
            'photo_url.required' => 'Link do zdjęcia jest wymagany',
            'photo_url.url' => 'Link do zdjęcia powinien być prawidłowy',
        ];
    }

}
