<?php

namespace App\Http\Controllers;

use App\Album;
use App\Http\Requests\AlbumPost;
use App\Http\Requests\AlbumRemovePost;

class AlbumController extends Controller
{

    /**
     * Get the List of Albums, order by latest
     * 
     * return View
     */
    public function getList()
    {
        $albums = Album::with('Photos')->orderBy('created_at', 'desc')->get();

        return view('index')->with('albums', $albums);
    }

    /**
     * Get the Album creating form
     * 
     * @return View
     */
    public function getForm()
    {
        return view('album_create');
    }

    /**
     * Handling of the Album creating form.
     * Saving new Album
     * 
     * @param AlbumPost $request - the object after Validation
     * @return RedirectResponse
     */
    public function postForm(AlbumPost $request)
    {
        $album = Album::create(array(
                    'name' => $request->input('name')
        ));

        return redirect()->route('album_show', [$album]);
    }

    /**
     * Handling of the Removing Album "form"
     * 
     * @param AlbumRemovePost $request - object after Validation
     * @return type
     */
    public function remove(AlbumRemovePost $request)
    {
        $album = Album::find($request->input('album_id'));
        $album->delete();

        return redirect()->route('index');
    }

    /**
     * Show the Album with photos
     * 
     * @param int $id
     */
    public function showAlbum($id)
    {
        $album = Album::with('Photos')->find($id);

        return view('album_show')->with('album', $album);
    }

}
