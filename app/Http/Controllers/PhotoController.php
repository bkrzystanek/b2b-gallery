<?php

namespace App\Http\Controllers;

use App\Album;
use App\Photo;
use App\Http\Requests\PhotoPost;

class PhotoController extends Controller
{

    /**
     * Get the Photo creating form
     * 
     * @return View
     */
    public function getForm($id)
    {
        $album = Album::find($id);

        return view('photo_add')->with('album', $album);
    }

    /**
     * Handling of the Photo adding form.
     * Saving Photo to Album
     * 
     * @param PhotoPost $request - the object after Validation
     * @return RedirectResponse
     */
    public function postForm(PhotoPost $request)
    {
        Photo::create(array(
            'photo_url' => $request->input('photo_url'),
            'album_id' => $request->input('album_id')
        ));

        $album = Album::find($request->input('album_id'));

        return redirect()->route('album_show', [$album]);
    }

}
